import Tkinter as tk
import tkMessageBox
def KlikBind(zdarz = None):
	if zdarz == None:
		tkMessageBox.showinfo("Tkinter - zdarzenia", "Klik przez bind!")
	
	else:
		s = "x = "+str(zdarz.x)+"y = "+str(zdarz.y)+" num = "+str(zdarz.num)+ "typ="+str(zdarz.type)
		tkMessageBox.showinfo("Tkinter - zdarzenia", "Klik przez bind!"+s)
	

def Klik():
	global MainWindow
	message=tkMessageBox.askyesno("Ostrzezenie", "czy na pewno chcesz zamknac?")
	if message == True:
		MainWindow.destroy()
	else:
		odp=tkMessageBox.showwarning("Dobry wybor!", "kontynuuj")



MainWindow = tk.Tk()
MainWindow.title("MY FIRST GUI IN PYTHON 2.7")
MainWindow.geometry("500x500")


menubar = tk.Menu(MainWindow)
filemenu = tk.Menu(menubar, tearoff=0)
filemenu.add_command(label="New", command=Klik)
filemenu.add_separator()
filemenu2 = tk.Menu(menubar, tearoff=0)

filemenu2.add_command(label="opcje", command=Klik)
filemenu2.add_command(label="pomoc", command=Klik)
filemenu.add_command(label="zapisz", command=KlikBind)

menubar.add_cascade(label="Plik", menu=filemenu)
menubar.add_cascade(label="Inne", menu=filemenu2)

w = tk.Label(MainWindow, text="siema", fg="light green", bg="yellow")
button = tk.Button(MainWindow, text='stop', width=15, command=Klik)

b1 = tk.Button(MainWindow, text="Costam1").pack()
b2 = tk.Button(MainWindow, text="Costam2").pack()
b3 = tk.Button(MainWindow, text="Costam3").pack()
b4 = tk.Button(MainWindow, text="Costam4").pack()
b5 = tk.Button(MainWindow, text="Costam5").pack()
b6 = tk.Button(MainWindow, text="Costam6").pack()
b7 = tk.Button(MainWindow, text="Costam7").pack()
b8 = tk.Button(MainWindow, text="Costam8").pack()

Ramka= tk.Frame(MainWindow,height=30,width=100,bg="blue")
Ramka.bind("<Button-1>", KlikBind)
Ramka.pack()
przelacznik = 0
tk.Checkbutton(MainWindow,text="Przelacznik",variable=przelacznik).pack()
tk.Radiobutton(MainWindow,text="Kawa",variable=przelacznik,value=1).pack()
tk.Radiobutton(MainWindow,text="Herbata",variable=przelacznik,value=0).pack()
w.pack(side="right")
MainWindow.config(menu=menubar)
MainWindow.mainloop()

